return unless ENV['GENERATE_WEEKLY_UPDATE'] || ENV['CI_JOB_STAGE'] == 'test'

require 'date'
require 'delegate'
require 'erb'
require 'gitlab'
require 'httparty'
require 'json'
require 'nokogiri'
require 'set'
require 'yaml'

require_relative 'capacity'
require_relative 'contribution'
require_relative 'description'
require_relative 'error_budget'
require_relative 'issue'
require_relative 'issue_distribution'
require_relative 'janky_cache'
require_relative 'markdown'
require_relative 'milestone_plan'
require_relative 'team'
require_relative 'team_activity'
require_relative 'weekly_update'

# Generates a summary issue of Monitor:Respond group acitivities occured during
# the previous week & planned for the upcoming week.
class Generator
  attr_reader :project_id, :today, :options, :gitlab_api

  def initialize(project_id, options)
    @project_id = project_id
    @today = Date.today
    @options = options
  end

  def run(api_token)
    puts "Running weekly update for #{today.to_s} - W#{today.cweek}"

    connect(api_token)
    skip_sections!
    generate!

    puts 'Run Complete.'
  end

  def connect(api_token)
    puts 'Connecting to GitLab...'

    @gitlab_api = Gitlab.client(
      endpoint: 'https://gitlab.com/api/v4',
      private_token: api_token
    )

    puts "Connection successful - API: authenticated as #{@gitlab_api.user.username}"
  end

  def skip_sections!
    puts "\nSkipping sections: #{options[:skip].join(', ')}.\n"
    Description.skip_sections(options[:skip])
  end

  def generate!
    current_issue = WeeklyUpdate.new(gitlab_api, today, project_id)
    next_issue = WeeklyUpdate.new(gitlab_api, today.next_day(7), project_id)

    find_or_create(current_issue)
    find_or_create(next_issue)
    update(current_issue)

    add_comments(next_issue)
    add_comments(current_issue)
  end

  def find_or_create(update_issue)
    if update_issue.current.nil?
      issue = update_issue.create!(dry_run: options[:dry_run])
      log_issue(issue, 'Created') if issue
    else
      log_issue(update_issue.current, 'Found')
    end
  end

  def update(update_issue)
    issue = update_issue.update!(dry_run: options[:dry_run])
    log_issue(issue, 'Updated')
  end

  def add_comments(update_issue)
    comments = update_issue.comment!(dry_run: options[:dry_run])
    comments.map { log_comment(_1) }
  end

  def log_issue(issue, prefix)
    puts "\n\n\n\n"
    puts "#{prefix} issue #{issue.title}: ##{issue.id} - #{issue.web_url}"
    puts "\n\n"
    puts issue.description
    puts "\n\n\n\n"
  end

  def log_comment(comment)
    puts "\n\n\n\n"
    puts "Threads added for issue:\n"
    puts comment.notes.first.body
    puts "\n\n\n\n"
  end
end

if $PROGRAM_NAME == __FILE__
  begin
    project_id = ENV['CI_PROJECT_ID']
    api_token = ENV['GITLAB_API_PRIVATE_TOKEN']
    options = { skip: [] }

    ARGV.each do |arg|
      skip = arg.match(/--skip-(?<section>[\w-]+\b)/)
      options[:skip] << skip['section'].gsub('_', '-').to_sym if skip
      options[:dry_run] = true if arg == '--dry-run'
    end

    Generator.new(project_id, options).run(api_token)
  rescue StandardError => e
    puts "#{e.class}: #{e.message}"
    puts e.backtrace
    raise
  end
end