# Represents team-specific configuration for generating the weekly update
class Team
  class << self
    CONFIG = YAML.load(ERB.new(File.read('config/weekly_update.yml')).result)

    def all
      @@all ||= CONFIG['engineers'].values.flatten.uniq
    end

    def frontend
      @@frontend ||= CONFIG.dig('engineers', 'frontend')
    end

    def backend
      @@backend ||= CONFIG.dig('engineers', 'backend')
    end

    def fullstack
      @@fullstack ||= CONFIG.dig('engineers', 'fullstack')
    end

    def manager
      @@manager ||= CONFIG.dig('engineers', 'manager')
    end

    def frontend_borrow
      CONFIG.dig('borrow', 'frontend') || 0
    end

    def backend_borrow
      CONFIG.dig('borrow', 'backend') || 0
    end

    def group
      CONFIG['group_label']
    end

    def planning_issue_project
      CONFIG['planning_issue_project'] || []
    end

    def availability_url
      CONFIG['error_budget_availability']
    end

    def capacity_url
      CONFIG['weekly_capacity']
    end
  end
end