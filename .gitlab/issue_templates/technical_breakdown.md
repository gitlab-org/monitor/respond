<!-- 
Using this template: 
  - Add or remove any content as needed. This is meant to be a convenient starting point, not a required structure. The goal is to highlight & research unknowns or dependencies, align the team on a technical approach, raise any technical limitations early, and ensure all implementation tasks are represented by atomic issues.
  - Post any open questions as individual threads on the issue. When a decision is made, update the original comment with the answer and react with ✅. If relevant, update the issue description or any other related issues.
-->

This is the technical breakdown for <LINK TO FEATURE EPIC/ISSUE>.

# Architecture

<!-- How does data flow through the application? Where is that data stored? -->
<!-- What are the entry points to the flow? User actions? Scheduled tasks? -->

### Assumptions & Limitations

<!-- Is there anything we're expressly excluding from the scope? -->
<!-- Is the approach going to make certain tasks more difficult down the line? -->
<!-- Is the approach based on any assumptions about other team's work or goals? -->


### Permissions & Security
<!-- Who has permission to take which actions? At which points will that be enforced? -->
<!-- Which tier should this belong to? -->

### Frontend
<!-- Can we utilize any existing components or UX? -->
<!-- How will the new components be organized? -->
<!-- Do the designs cover both mobile & desktop use cases? Is there anything additional which needs to be addressed for either desktop or mobile versions? -->
<!-- Are all error states defined? What types of errors are expected from the server? What will be presented in the UI? -->

### Backend
##### Database

<!-- Do we need any new tables? Columns on existing tables? -->
<!-- Which indicies do we think are needed? Which columns will we search/filter by? -->
<!-- Which columns can be null? Do we need to include a UNIQUE index? -->
<!-- Which validations do we need on the model? Presence checks? -->

- *Table:* `my_new_table`
- *Model:* `MyNewTable`

| Column | Type | Required? | Description |
|--------|------|-----------|-------------|
|        |      |           |             |

Validations & Constraints:
- <!-- Sample constraint -->


##### API

<!-- Do we need any new graphql queries or mutations? Additions to existing types? -->
<!-- Do we need any new REST endpoints? Additions to existing endpoints? -->
<!-- Are there are new pages which require Rails controllers? -->
<!-- How will we manage any deprecations? -->
<!-- How can we ensure these requests are performant? -->


*Sample <New> Query*
```graphql
```

*Sample <New> Input*
```json
```

##### Scheduled/Async jobs

<!-- Which tasks should be handled in the background or on a schedule? -->
<!-- How frequently & urgently should these tasks be completed? -->
<!-- Which events should correspond to a system note? -->
<!-- Which events should trigger an email being sent? -->




# Rollout
<!-- Is there work which needs to be completed in different releases? -->
<!-- How long do we need to ensure the feature has been sufficiently tested? -->

### Feature flag
<!-- Is a feature flag needed? -->
<!-- Which pieces need to be complete in order to release the feature flag? -->
<!-- Should there be a Monitor:Respond-specific design review issue corresponding to the feature flag issue? -->


### Testing

#### Risks
<!-- What risks does this change pose to our availability? -->
<!-- How might it affect the quality of the product? -->
<!-- Will this require cross-browser testing? -->
<!-- Is this a breaking change to existing user workflows? -->

#### Expected white-box tests
<!-- Which feature/system test cases should we cover?
          ex) sample test case outline: https://gitlab.com/gitlab-org/gitlab/-/issues/386763 -->
<!-- Is there functionality that is not testable using unit tests?
          ex) Clicking a tab changes the url & renders the correct page
          ex) A frontend app is initialized with expected data from haml attributes -->
<!-- Docs on white-box testing: https://docs.gitlab.com/ee/development/testing_guide/testing_levels.html#white-box-tests-at-the-system-level-formerly-known-as-system--feature-tests -->


### Metrics
<!-- How will we track usage of this feature? -->
<!-- How will we monitor performance of new tables/endpoints/pages?-->

### Documentation
<!-- Where will the documentation of this feature live? -->
<!-- Is there additional documentation needed beyond basic usage of the UI? For example, adding a link from an index page or a how-to page for a common use-case? -->


# Issue breakdown
<!-- Are there individual pieces of the feature that can provide independent value? -->
<!-- Can issues be redefined so that items can be done in parallel? -->
<!-- Do many issues depend on the same piece of work being done? -->

<!-- Mark Backend/Frontend columns with ✅ and/or a weight estimate. -->
<!-- Add the issue link to the table once the issue has been created. -->

| Issue  | Notes | Backend | Frontend |
|--------|-------|---------|----------|
|        |       |         |          |

<!-- 

For new issues: 
- [ ] Add section/stage/group/category/feature labels
- [ ] Add relevant frontend/backend labels as needed
- [ ] Set appropriate blocking linked issues
- [ ] Add to relevant epic
- [ ] Add detailed description, including: 
    - What: What is the goal of the issue?
    - Why: What is the motivation? Is there additional context which can be linked?
    - How: What is in scope? Out of scope? Anything blocking?
    - Designs: Pull relevant designs directly into the issue description for easy reference. 

-->



# Definition of done

The following are required prior to starting implementation:

- [ ] Draft of breakdown issue was posted to `#g_respond` with a request for feedback
- [ ] At least one engineer provided feedback
- [ ] Scope, timeline/dependencies, and technical limitations are fully defined
- [ ] Detailed breakdown issue was posted to `#g_respond` with a last call for feedback
- [ ] All needed issues are created & linked to the appropriate epic
- [ ] All open threads are resolved or determined non-blocking


/label ~"group::respond" ~"devops::monitor" ~"section::ops"
